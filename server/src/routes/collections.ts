import { Router } from 'express';
import { collectionController as CollectionController } from '../controller/Collection.controller';

const router = Router();

router.get('/', CollectionController.getAllByShop);
router.get('/:id/products', CollectionController.getProducts);
router.post('/create', CollectionController.create);
router.patch('/update', CollectionController.update);

export default router;
