import { Router } from 'express';
import { User } from '../models';
import {
  WEBHOOK_APP_UNINSTALLED,
  WEBHOOK_SHOP_UPDATE,
} from '../utils/constants/webhooks';

const router = Router();

router.post(WEBHOOK_APP_UNINSTALLED.endpoint, async (req, res) => {
  const { myshopify_domain } = req.body;
  if (!myshopify_domain) return;

  try {
    const now = Date.now();
    await User.findOneAndUpdate(
      { domain: myshopify_domain },
      { password: '', uninstalledAt: now }
    );
  } catch (error) {
    // log failed webhook
  }
  return res.status(200).send('Webhook recieved successfully');
});

router.post(WEBHOOK_SHOP_UPDATE.endpoint, async (req, res) => {
  const { myshopify_domain } = req.body;

  if (!myshopify_domain) return;

  try {
    const now = Date.now();
    await User.findOneAndUpdate(
      { domain: myshopify_domain },
      {
        name: req.body.name,
        email: req.body.email,
        id: req.body.id,
        uninstalledAt: now,
      }
    );
  } catch (error) {
    // log failed webhook
  }
  return res.status(200).send('Webhook recieved successfully');
});

export default router;
