import { Router } from 'express';
import { AuthController } from '../controller/Auth.controller';

const router = Router();

router.post('/check-me', AuthController.checkAuth);
router.post('/retrieve-token', AuthController.retrieveToken);

export default router;
