import { Router } from 'express';
import { productController as ProductController } from '../controller/Product.controller';

const router = Router();

router.post('/create', ProductController.create);
router.patch('/update', ProductController.update);

export default router;
