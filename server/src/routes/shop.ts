import { Router } from 'express';
import { ShopController } from '../controller/Shop.controller';

const router = Router();

router.get('/', ShopController.getShopDetails);

export default router;
