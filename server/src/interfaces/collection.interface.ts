export interface ICollection {
  name: string;
  description: string;
  shopId: string;
}
