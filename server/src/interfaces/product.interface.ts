export interface IProduct {
  name: string;
  description: string;
  collectionId: string;
}
