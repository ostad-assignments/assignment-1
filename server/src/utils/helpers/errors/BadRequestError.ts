export class BadRequestError extends Error {
  code = 400;

  constructor() {
    super('Bad request');
  }
}
