export class NotFoundError extends Error {
  code = 404;

  constructor(public message = 'Not found') {
    super(message);
  }
}
