export const WEBHOOK_APP_UNINSTALLED = {
  topic: 'APP_UNINSTALLED',
  endpoint: '/app-uninstalled',
};
export const WEBHOOK_SHOP_UPDATE = {
  topic: 'SHOP_UPDATE',
  endpoint: '/shop-updated',
};
