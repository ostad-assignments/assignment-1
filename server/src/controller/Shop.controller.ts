import { NextFunction, Request, Response } from 'express';
import { User } from '../models';
import { BadRequestError } from '../utils/helpers/errors/BadRequestError';
import { NotFoundError } from '../utils/helpers/errors/NotFoundError';

class Shop {
  async getShopDetails(req: Request, res: Response, next: NextFunction) {
    const { domain } = req.user!;

    try {
      if (!domain) throw new BadRequestError();

      const user = await User.findOne({
        domain: domain,
      });

      if (!user) {
        throw new NotFoundError('No shop found');
      }

      return res.json({
        _id: user._id,
        name: user?.name,
        shopId: user?.shopId,
      });
    } catch (error) {
      next(error);
    }
  }
}

export const ShopController = new Shop();
