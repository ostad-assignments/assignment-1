import { NextFunction, Request, Response } from 'express';
import { IProduct } from '../interfaces';
import Product from '../models/product.model';
import { BadRequestError } from '../utils/helpers/errors/BadRequestError';

class ProductController {
  async create(
    req: Request<{}, {}, IProduct>,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { name, description, collectionId } = req.body;

      if (!name) throw new BadRequestError();

      const product = await Product.create({
        name,
        description,
        collectionId,
      });

      return res.json({ product });
    } catch (error) {
      console.log('error.message', error.message);
      next(error);
    }
  }

  async update(
    req: Request<
      {},
      {},
      { id: string; data: Partial<Omit<IProduct, 'collectionId'>> }
    >,
    res: Response,
    next: NextFunction
  ) {
    const { data, id } = req.body;

    if (!id) throw new BadRequestError();

    try {
      await Product.findByIdAndUpdate(id, {
        $set: data,
      });

      const updatedProduct = await Product.findById(id);

      return res.json({ product: updatedProduct });
    } catch (error) {
      next(error);
    }
  }
}

export const productController = new ProductController();
