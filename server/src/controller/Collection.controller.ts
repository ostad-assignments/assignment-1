import { NextFunction, Request, Response } from 'express';
import Collection from '../models/collection.model';
import { BadRequestError } from '../utils/helpers/errors/BadRequestError';
import { ICollection } from '../interfaces';
import Product from '../models/product.model';

class CollectionController {
  async getAllByShop(req: Request, res: Response, next: NextFunction) {
    try {
      const shopId = req.user?._id;
      const collections = await Collection.find({ shopId });

      return res.json({ collections });
    } catch (error) {
      next(error);
    }
  }

  async getProducts(req: Request, res: Response, next: NextFunction) {
    try {
      const { id } = req.params;
      const collection = await Collection.findById(id);

      if (
        !collection ||
        collection?.shopId.toString() !== req.user?._id!.toString()
      ) {
        throw new BadRequestError();
      }

      const products = await Product.find({ collectionId: id });

      return res.json({ products, collection });
    } catch (error) {
      next(error);
    }
  }

  async create(
    req: Request<{}, {}, Omit<ICollection, 'shopId'>>,
    res: Response,
    next: NextFunction
  ) {
    const { name, description } = req.body;

    try {
      if (!name) throw new BadRequestError();
      const shopId = req.user?._id;
      const collection = await Collection.create({
        name,
        description,
        shopId,
      });

      return res.json({ collection });
    } catch (error) {
      next(error);
    }
  }

  async update(
    req: Request<{}, {}, { id: string; data: Partial<ICollection> }>,
    res: Response,
    next: NextFunction
  ) {
    const { data, id } = req.body;

    if (!id) throw new BadRequestError();

    try {
      await Collection.findByIdAndUpdate(id, {
        $set: data,
      });

      const updatedCollection = await Collection.findById(id);

      return res.json({ collection: updatedCollection });
    } catch (error) {
      next(error);
    }
  }
}

export const collectionController = new CollectionController();
