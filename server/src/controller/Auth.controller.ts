import { NextFunction, Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import fetch from 'node-fetch';
import { User } from '../models';
import { registerWebhook } from '../services/webhooks';
import {
  JWT_SECRET,
  SHOPIFY_CLIENT_ID,
  SHOPIFY_CLIENT_SECRET,
} from '../utils/constants/global';
import {
  WEBHOOK_APP_UNINSTALLED,
  WEBHOOK_SHOP_UPDATE,
} from '../utils/constants/webhooks';
import { fetchGql } from '../services/fetch-gql';
import { BadRequestError } from '../utils/helpers/errors/BadRequestError';

class Auth {
  async checkAuth(req: Request, res: Response, next: NextFunction) {
    const { shop } = req.body;

    try {
      if (!shop) throw new BadRequestError();

      const user = await User.findOne({
        domain: shop,
        password: { $exists: true, $ne: '' },
      });

      if (!user) {
        return res.json({ authenticated: false });
      }

      const token = jwt.sign({ id: user._id, shop }, JWT_SECRET!);

      return res.json({ authenticated: true, token });
    } catch (error) {
      next(error);
    }
  }

  async retrieveToken(req: Request, res: Response, next: NextFunction) {
    const { shop: domain, code } = req.body;
    const tokenUrl = `https://${domain}/admin/oauth/access_token?client_id=${SHOPIFY_CLIENT_ID}&client_secret=${SHOPIFY_CLIENT_SECRET}&code=${code}`;

    try {
      const data = await fetch(tokenUrl, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
      });

      const { access_token } = await data.json();
      let user = await User.findOne({ domain });
      const query = `query {
        shop {
          id
          name
          myshopifyDomain
          email
        }
      }`;

      const shopQuery = await fetchGql(
        { domain, password: access_token },
        query
      );

      const {
        data: { shop: store },
      } = await shopQuery.json();

      if (user) {
        user.password = access_token;
        user.name = store.name;
        user.email = store.email;
        user.id = store.id;
        user.save();
      } else {
        user = await User.create({
          name: store.name,
          shopId: store.id,
          email: store.email,
          domain: store.myshopifyDomain,
          password: access_token,
        });
      }

      const token = jwt.sign({ id: user._id, shop: domain }, JWT_SECRET!);

      // register webhook for uninstall event
      registerWebhook(
        { domain: user.domain, password: user.password },
        WEBHOOK_APP_UNINSTALLED.topic,
        WEBHOOK_APP_UNINSTALLED.endpoint
      );
      registerWebhook(
        { domain: user.domain, password: user.password },
        WEBHOOK_SHOP_UPDATE.topic,
        WEBHOOK_SHOP_UPDATE.endpoint
      );

      return res.json({ token });
    } catch (error) {
      next(error);
    }
  }
}

export const AuthController = new Auth();
