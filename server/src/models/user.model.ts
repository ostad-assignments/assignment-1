import mongoose, { Document } from 'mongoose';
import { IUser } from '../interfaces';

const userSchema = new mongoose.Schema<IUser & Document>(
  {
    name: String,
    email: String,
    shopId: String,
    domain: String,
    password: String,
    installedAt: { type: Date, default: Date.now },
    passwordUpdatedAt: { type: Date, default: Date.now },
    uninstalledAt: Date,
  },
  { timestamps: true }
);

export default mongoose.model('User', userSchema);
