import mongoose, { Document } from 'mongoose';
import { ICollection } from '../interfaces';
import User from './user.model';

const collectionSchema = new mongoose.Schema<ICollection & Document>(
  {
    name: String,
    description: String,
    shopId: { type: String, ref: User },
  },
  { timestamps: true }
);

export default mongoose.model('Collection', collectionSchema);
