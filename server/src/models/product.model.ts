import mongoose, { Document } from 'mongoose';
import { IProduct } from '../interfaces';
import Collection from './collection.model';

const productSchema = new mongoose.Schema<IProduct & Document>(
  {
    name: String,
    description: String,
    collectionId: { type: String, ref: Collection },
  },
  { timestamps: true }
);

export default mongoose.model('Product', productSchema);
