import { NextFunction, Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import { ObjectId } from 'mongoose';
import { User } from '../models';
import { JWT_SECRET } from '../utils/constants/global';
import { AppError } from '../utils/helpers/errors/AppError';

export const isAuthenticated = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const { authorization } = req.headers;

    if (!authorization) throw new AppError('Not authenticated', 401);

    const token = authorization.split(' ')[1];
    const decoded = jwt.verify(token, JWT_SECRET!) as {
      id: ObjectId;
      iat: number;
    };

    if (!decoded?.id) {
      throw new AppError('Not authenticated', 401);
    }

    const user = await User.findOne({ _id: decoded.id });
    if (!user) {
      throw new AppError('Not authenticated', 401);
    }

    req.user = { password: user.password, domain: user.domain, _id: user._id };
    next();
  } catch (error) {
    console.log('error', error.message);
    next(error);
  }
};
