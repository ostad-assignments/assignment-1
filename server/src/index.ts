import { config as dotenvConfig } from 'dotenv';
dotenvConfig();

import cors from 'cors';
import express, { NextFunction, Request, Response } from 'express';
import mongoose from 'mongoose';
import authRouter from './routes/auth';
import shopRouter from './routes/shop';
import collectionsRouter from './routes/collections';
import productsRouter from './routes/products';
import webhookRouter from './routes/webhooks';
import { DB_URL, PORT } from './utils/constants/global';
import { BadRequestError } from './utils/helpers/errors/BadRequestError';
import { NotFoundError } from './utils/helpers/errors/NotFoundError';
import { AppError } from './utils/helpers/errors/AppError';
import { isAuthenticated } from './middlewares/auth.middleware';

const dbUrl = DB_URL;
const port = PORT;
const app = express();

mongoose
  .connect(dbUrl!)
  .then(() => {
    console.log(`db connection established`);

    app.use(cors());
    app.use(express.json());

    app.get('/', (req, res) => {
      res.json({ a: 1, b: 2, c: 4 });
    });

    app.use('/auth', authRouter);
    app.use('/shop', isAuthenticated, shopRouter);
    app.use('/collections', isAuthenticated, collectionsRouter);
    app.use('/products', isAuthenticated, productsRouter);
    app.use('/webhooks', webhookRouter);

    app.use((error: Error, req: Request, res: Response, next: NextFunction) => {
      if (
        error instanceof BadRequestError ||
        error instanceof NotFoundError ||
        error instanceof AppError
      ) {
        console.log('error.message', error.message);
        return res.status(error.code).json({ error: error.message });
      }

      return res.status(500).json({ error: 'Something went wrong!' });
    });

    app.listen(port, () => {
      console.log(`app is listening to port ${port}`);
    });
  })
  .catch((error) => {
    console.log('error', error);
  });
