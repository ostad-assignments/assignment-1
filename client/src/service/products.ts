import { SERVER_URL } from '../utils/constants/global';
import { authenticatedFetch } from '../utils/helpers/fetch';
import { IProduct } from '../utils/interfaces/product.interface';

export const getProductsByCollection = async (collectionId: string) => {
  try {
    const url = `${SERVER_URL}/collections/${collectionId}/products`;
    const data = await authenticatedFetch(url, {
      method: 'GET',
    });

    if (!data.ok) {
      return { error: "Sorry! We couldn't find any products." };
    }

    return await data.json();
  } catch (error) {
    console.log('error in service', error);
  }
};

export const createProduct = async (data: IProduct) => {
  const url = `${SERVER_URL}/products/create`;

  try {
    const response = await authenticatedFetch(url, {
      method: 'POST',
      body: JSON.stringify(data),
    });

    if (!response.ok) {
      throw new Error();
    }

    const product = await response.json();

    return product;
  } catch (error) {
    return { error: 'Failed to create the product' };
  }
};

export const updateProduct = async (
  id: string,
  data: Omit<IProduct, 'collectionId'>
) => {
  const url = `${SERVER_URL}/products/update`;

  try {
    const response = await authenticatedFetch(url, {
      method: 'PATCH',
      body: JSON.stringify({ id, data }),
    });

    if (!response.ok) {
      throw new Error();
    }

    const product = await response.json();

    return product;
  } catch (error) {
    return { error: 'Failed to update the product' };
  }
};
