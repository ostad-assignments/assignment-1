import { SERVER_URL } from '../utils/constants/global';
import { authenticatedFetch } from '../utils/helpers/fetch';

export const getShopDetails = async () => {
  try {
    const url = `${SERVER_URL}/shop`;

    const data = await authenticatedFetch(url, { method: 'GET' });

    if (!data.ok) {
      return { error: "Sorry! We couldn't find the shop details." };
    }

    return await data.json();
  } catch (error) {
    console.log('error in service', error);
  }
};
