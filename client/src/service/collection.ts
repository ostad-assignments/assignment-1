import { SERVER_URL } from '../utils/constants/global';
import { authenticatedFetch } from '../utils/helpers/fetch';

export const getCollections = async () => {
  try {
    const url = `${SERVER_URL}/collections`;
    const data = await authenticatedFetch(url, {
      method: 'GET',
    });

    if (!data.ok) {
      return { error: "Sorry! We couldn't find any collection." };
    }

    return await data.json();
  } catch (error) {
    console.log('error in service', error);
  }
};

export const createCollection = async ({
  name,
  description,
}: {
  name: string;
  description: string;
}) => {
  try {
    const url = `${SERVER_URL}/collections/create`;

    const data = await authenticatedFetch(url, {
      method: 'POST',
      body: JSON.stringify({ name, description }),
    });

    if (!data.ok) throw new Error();

    return data.json();
  } catch (error) {
    return { error: 'Failed to create the product' };
  }
};

export const updateCollection = async ({
  id,
  name,
  description,
}: {
  id: string;
  name: string;
  description: string;
}) => {
  const url = `${SERVER_URL}/collections/update`;

  const data = await authenticatedFetch(url, {
    method: 'PATCH',
    body: JSON.stringify({ id, data: { name, description } }),
  });

  return data.json();
};
