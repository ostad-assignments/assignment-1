import { Banner, BannerProps, Text } from '@shopify/polaris';
import { FC } from 'react';

const ErrorComp: FC<{
  errorText: string;
  bannerAction: Pick<BannerProps, 'action'>;
}> = ({ errorText, bannerAction: { action } }) => {
  return (
    <>
      <Banner
        title="Oops!"
        tone="critical"
        action={{ content: 'Reload', onAction: action?.onAction }}
      >
        <Text variant="headingXl" as="h1">
          {errorText}
        </Text>
      </Banner>
    </>
  );
};

export default ErrorComp;
