import {
  BlockStack,
  Frame,
  Modal,
  ModalProps,
  TextField,
} from '@shopify/polaris';
import { FC, useState } from 'react';

const CreateModal: FC<
  ModalProps & {
    primaryActionHandler: (name: string, description: string) => Promise<void>;
    defaultName?: string;
    defaultDescription?: string;
  }
> = ({
  title,
  onClose,
  open,
  primaryAction,
  primaryActionHandler,
  defaultDescription = '',
  defaultName = '',
  secondaryActions,
}) => {
  const [name, setName] = useState(defaultName);
  const [description, setDescription] = useState(defaultDescription);

  const handleNameChange = (value: string) => {
    setName(value);
  };
  const handleDescriptionChange = (value: string) => {
    setDescription(value);
  };

  const handleSubmit = () => {
    primaryActionHandler(name, description);
    onClose();
  };

  return (
    <Frame>
      <Modal
        title={title}
        open={open}
        onClose={onClose}
        primaryAction={{
          ...primaryAction,
          onAction: handleSubmit,
          disabled: !name,
        }}
        secondaryActions={secondaryActions}
      >
        <Modal.Section>
          <BlockStack gap="200">
            <TextField
              label="Name *"
              autoComplete="off"
              value={name}
              onChange={handleNameChange}
            />

            <TextField
              label="Description"
              autoComplete="off"
              multiline={4}
              maxHeight={200}
              value={description}
              onChange={handleDescriptionChange}
            />
          </BlockStack>
        </Modal.Section>
      </Modal>
    </Frame>
  );
};

export default CreateModal;
