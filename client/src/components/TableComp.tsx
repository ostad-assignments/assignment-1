import {
  Button,
  ButtonGroup,
  Card,
  Frame,
  IndexTable,
  Text,
} from '@shopify/polaris';
import { IndexTableBaseProps } from '@shopify/polaris/build/ts/src/components/IndexTable';
import { FC, useMemo } from 'react';
import { ICollection } from '../utils/interfaces/collection.interface';
import { IProduct } from '../utils/interfaces/product.interface';

type Headings = {
  [k in keyof Pick<IndexTableBaseProps, 'headings'>]: Pick<
    IndexTableBaseProps,
    'headings'
  >[k];
}[keyof Pick<IndexTableBaseProps, 'headings'>];

type Data = ICollection | IProduct;

export type TableAction = {
  show: boolean;
  viewText: string;
  editText: string;
  onViewClick: (id: string) => void;
  onEditClick: (id: string) => void;
};

const TableComp: FC<{
  data: Data[];
  action: TableAction;
}> = ({ data, action }) => {
  const headings: Headings = useMemo(() => {
    const heads = [{ title: 'Name' }, { title: 'Description' }] as Headings;
    if (action.show) {
      heads.push({ title: '' });
    }

    return heads;
  }, [action.show]);

  const rowMarkup = data.map(({ _id, name, description }, index) => {
    const handleEditClick = () => {
      if (!_id) return;

      action.onEditClick(_id);
    };
    const handleViewClick = () => {
      if (!_id) return;

      action.onViewClick(_id);
    };

    return (
      <IndexTable.Row id={_id!} key={_id} position={index}>
        <IndexTable.Cell>
          <Text variant="bodyMd" fontWeight="bold" as="span">
            {name}
          </Text>
        </IndexTable.Cell>
        <IndexTable.Cell>
          <Text variant="bodyMd" fontWeight="regular" as="p" truncate={false}>
            {description || '-'}
          </Text>
        </IndexTable.Cell>

        {!action.show ? null : (
          <IndexTable.Cell>
            <ButtonGroup>
              {action.editText ? (
                <Button onClick={handleEditClick}>{action.editText}</Button>
              ) : null}
              {action.viewText ? (
                <Button onClick={handleViewClick}>{action.viewText}</Button>
              ) : null}
            </ButtonGroup>
          </IndexTable.Cell>
        )}
      </IndexTable.Row>
    );
  });

  return (
    <Frame>
      <Card>
        <IndexTable
          headings={headings}
          itemCount={data.length}
          selectable={false}
        >
          {rowMarkup}
        </IndexTable>
      </Card>
    </Frame>
  );
};

export default TableComp;
