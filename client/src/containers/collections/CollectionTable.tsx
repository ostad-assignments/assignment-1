import { FC, useMemo } from 'react';
import TableComp, { TableAction } from '../../components/TableComp';
import { ICollection } from '../../utils/interfaces/collection.interface';

const CollectionTable: FC<{ data: ICollection[]; action: TableAction }> = ({
  data,
  action,
}) => {
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const collections = useMemo(() => data, [data.length]);

  return <TableComp data={collections} action={action} />;
};

export default CollectionTable;
