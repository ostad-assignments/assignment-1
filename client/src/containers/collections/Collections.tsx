import {
  Card,
  EmptyState,
  Frame,
  Page,
  Spinner,
  Toast,
} from '@shopify/polaris';
import { useCallback, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import CreateModal from '../../components/CreateModal';
import ErrorComp from '../../components/ErrorComp';
import { useGetCollectionList } from '../../hooks/collection';
import { createCollection, updateCollection } from '../../service/collection';
import { handleReload } from '../../utils/helpers/url';
import { ICollection } from '../../utils/interfaces/collection.interface';
import CollectionTable from './CollectionTable';

const Collections = () => {
  const { collections, loading, error } = useGetCollectionList();
  const navigate = useNavigate();

  const [collectionList, setCollectionList] = useState<ICollection[]>([]);
  const [showCreateModal, setShowCreateModal] = useState(false);
  const [showEditModal, setShowEditModal] = useState(false);
  const [selectedCollection, setSelectedCollection] =
    useState<ICollection | null>(null);

  const [showToast, setShowToast] = useState(false);
  const [toastMessage, setToastMessage] = useState('');
  const [toastErrorStatus, setToastErrorStatus] = useState(false);

  const handleCreateBtnClick = () => {
    setShowCreateModal(true);
  };

  const handleCreateModalClose = () => {
    setShowCreateModal(false);
  };

  const handleEditModalShow = (id: string) => {
    const collection = collectionList.find(
      (collection) => collection._id === id
    );

    if (!collection) {
      setToastMessage('Something went wrong!');
      setToastErrorStatus(true);
      setShowToast(true);

      return;
    }

    setSelectedCollection(collection);

    setShowEditModal(true);
  };

  const handleEditModalClose = () => {
    setSelectedCollection(null);
    setToastErrorStatus(false);
    setToastMessage('');
    setShowEditModal(false);
  };

  const handleCreateCollection = async (name: string, description: string) => {
    try {
      const response = await createCollection({ name, description });

      if (response.error) {
        throw new Error();
      }

      setCollectionList((prev) => [...prev, response.collection]);
      setToastMessage(`Collection created.`);
      setShowToast(true);
    } catch (error) {
      setToastMessage('Failed to create collection.');
      setShowToast(true);
      setToastErrorStatus(true);
    }
  };

  const handleEditCollection = async (name: string, description: string) => {
    if (!selectedCollection) throw new Error('No collection is selected');

    try {
      const { collection } = await updateCollection({
        id: selectedCollection._id!,
        name,
        description,
      });

      setCollectionList((prev) => {
        const index = prev.findIndex(
          (item) => item._id === selectedCollection._id
        );

        if (index < 0) {
          return prev;
        }

        prev.splice(index, 1, collection);
        return prev;
      });

      setToastMessage(`Collection updated.`);
      setShowToast(true);
      handleEditModalClose();
    } catch (error) {
      setToastMessage('Failed to create collection.');
      setShowToast(true);
      setToastErrorStatus(true);
    }
  };

  const handleProductRedirect = (id: string) => {
    const url = `/collections/${id}/products`;

    navigate(url);
    return;
  };

  const onToastDismiss = () => {
    setShowToast(false);
    setToastErrorStatus(false);
    setToastMessage('');
  };

  const compRenderer = useCallback(() => {
    if (error) {
      return (
        <ErrorComp
          errorText={error}
          bannerAction={{
            action: { content: 'Reload', onAction: handleReload },
          }}
        />
      );
    }

    if (loading) {
      return <Spinner />;
    }

    if (!collectionList.length)
      return (
        <Card>
          <EmptyState
            heading="No collection found."
            action={{
              content: 'Add new collection',
              onAction: handleCreateBtnClick,
            }}
            image="https://cdn.shopify.com/s/files/1/0262/4071/2726/files/emptystate-files.png"
          >
            <p>
              There is no collection for this collection. You can add new
              collection.
            </p>
          </EmptyState>
        </Card>
      );

    return (
      <CollectionTable
        data={collectionList}
        action={{
          show: true,
          editText: 'Edit',
          onEditClick: handleEditModalShow,
          viewText: 'Product',
          onViewClick: handleProductRedirect,
        }}
      />
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [error, loading, collectionList.length]);

  useEffect(() => {
    if (collections.length && collectionList.length !== collections.length) {
      setCollectionList(collections);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [collections?.length]);

  return (
    <Frame>
      <Page
        title="Collections"
        primaryAction={{
          content: 'Create collection',
          onAction: handleCreateBtnClick,
        }}
      >
        {compRenderer()}
      </Page>
      {showCreateModal ? (
        <CreateModal
          primaryActionHandler={handleCreateCollection}
          open={showCreateModal}
          title="Create collection"
          onClose={handleCreateModalClose}
          primaryAction={{
            content: 'Create',
          }}
          secondaryActions={[
            { content: 'Cancel', onAction: handleCreateModalClose },
          ]}
        />
      ) : null}

      {showEditModal ? (
        <CreateModal
          defaultName={selectedCollection?.name || ''}
          defaultDescription={selectedCollection?.description || ''}
          primaryActionHandler={handleEditCollection}
          open={showEditModal}
          title="Edit collection"
          onClose={handleEditModalClose}
          primaryAction={{
            content: 'Update',
          }}
          secondaryActions={[
            { content: 'Cancel', onAction: handleEditModalClose },
          ]}
        />
      ) : null}

      {showToast ? (
        <Toast
          error={toastErrorStatus}
          content={toastMessage}
          onDismiss={onToastDismiss}
          duration={3000}
        />
      ) : null}
    </Frame>
  );
};

export default Collections;
