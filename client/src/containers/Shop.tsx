import {
  Banner,
  Card,
  DescriptionList,
  Page,
  Spinner,
  Text,
} from '@shopify/polaris';
import { useShopInfo } from '../hooks/shop';
import { handleReload } from '../utils/helpers/url';

const Shop = () => {
  const { shop, loading, error } = useShopInfo();

  if (loading) <Spinner />;

  if (error)
    return (
      <Page title="Shop">
        <Banner
          title="Oops!"
          tone="critical"
          action={{ content: 'Reload', onAction: handleReload }}
        >
          <Text variant="headingXl" as="h1">
            {error}
          </Text>
        </Banner>
      </Page>
    );

  return (
    <Page title="Shop">
      <Card>
        <DescriptionList
          items={[
            { term: 'Name', description: shop?.name },
            { term: 'Id', description: shop?.shopId?.split('/')?.pop() },
          ]}
        />
      </Card>
    </Page>
  );
};

export default Shop;
