import { FC, useMemo } from 'react';
import TableComp, { TableAction } from '../../components/TableComp';
import { IProduct } from '../../utils/interfaces/product.interface';

const ProductsTable: FC<{ data: IProduct[]; action: TableAction }> = ({
  data,
  action,
}) => {
  const products = useMemo(() => data, [data.length]);

  return <TableComp data={products} action={action} />;
};

export default ProductsTable;
