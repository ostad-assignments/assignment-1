import {
  Banner,
  Card,
  EmptyState,
  Frame,
  Page,
  Spinner,
  Text,
  Toast,
} from '@shopify/polaris';
import { useNavigate, useParams } from 'react-router-dom';
import { useGetProductsByCollection } from '../../hooks/products';
import { useEffect, useState } from 'react';
import CreateModal from '../../components/CreateModal';
import { IProduct } from '../../utils/interfaces/product.interface';
import ProductsTable from './ProductsTable';
import { createProduct, updateProduct } from '../../service/products';

const Products = () => {
  const { id } = useParams();
  const navigate = useNavigate();
  const { products, collection, loading, error } = useGetProductsByCollection(
    id!
  );

  const [productList, setProductList] = useState<IProduct[]>([]);
  const [showAddModal, setShowAddModal] = useState(false);
  const [showEditModal, setShowEditModal] = useState(false);
  const [selectedProduct, setSelectedProduct] = useState<IProduct | null>(null);

  const [showToast, setShowToast] = useState(false);
  const [toastMessage, setToastMessage] = useState('');
  const [toastErrorStatus, setToastErrorStatus] = useState(false);

  const handleBack = () => {
    navigate('/collections');
  };

  const onToastDismiss = () => {
    setShowToast(false);
    setToastErrorStatus(false);
    setToastMessage('');
  };

  const handleHideAddModal = () => {
    setShowAddModal(false);
  };

  const handleEditModalClose = () => {
    setSelectedProduct(null);
    setToastErrorStatus(false);
    setToastMessage('');
    setShowEditModal(false);
  };

  const handleShowAddModal = () => {
    setShowAddModal(true);
  };

  const handleShowEditModal = (id: string) => {
    const product = productList.find((p) => p._id === id);
    if (!product) {
      setToastMessage('Something went wrong!');
      setToastErrorStatus(true);
      setShowToast(true);

      return;
    }

    setSelectedProduct(product);
    setShowEditModal(true);
  };

  const handleAddProduct = async (name: string, description: string) => {
    try {
      const response = await createProduct({
        name,
        description,
        collectionId: collection!._id!,
      });

      if (response.error) {
        throw new Error();
      }

      setProductList((prev) => [...prev, response.product]);
      setToastMessage('Product added.');
      setShowToast(true);
    } catch (error) {
      setToastMessage('Failed to add product.');
      setToastErrorStatus(true);
      setShowToast(true);
    }
  };

  const handleEditProduct = async (name: string, description: string) => {
    try {
      if (!selectedProduct) throw new Error();

      const response = await updateProduct(selectedProduct._id!, {
        name,
        description,
      });

      if (response.error) {
        throw new Error();
      }

      setProductList((prev) => {
        const index = prev.findIndex(
          (item) => item._id === selectedProduct._id
        );

        if (index < 0) {
          return prev;
        }

        prev.splice(index, 1, response.product);
        return prev;
      });
      setToastMessage('Product added.');
      setShowToast(true);
      setSelectedProduct(null);
    } catch (error) {
      setToastMessage('Failed to add product.');
      setToastErrorStatus(true);
      setShowToast(true);
    }
  };

  useEffect(() => {
    if (products?.length !== productList.length) {
      setProductList(products);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [products.length]);

  if (loading) {
    return (
      <Page title="Products" backAction={{ onAction: handleBack }}>
        <Spinner />
      </Page>
    );
  }

  if (error) {
    return (
      <Page title="Products" backAction={{ onAction: handleBack }}>
        <Banner tone="critical" title="Oops!">
          <Text as="p" variant="bodyMd">
            {error}
          </Text>
        </Banner>
      </Page>
    );
  }

  if (!collection) {
    return (
      <Page title="Products" backAction={{ onAction: handleBack }}>
        <Banner title="Oops!" tone="critical">
          <Text variant="headingMd" as="h2">
            Sorry! No products found
          </Text>
        </Banner>
      </Page>
    );
  }

  return (
    <Frame>
      <Page
        title={`${collection.name}'s Products`}
        backAction={{ onAction: handleBack }}
        primaryAction={{ content: 'Add product', onAction: handleShowAddModal }}
      >
        {productList?.length ? (
          <ProductsTable
            data={productList}
            action={{
              show: true,
              viewText: '',
              onViewClick: () => {},
              editText: 'Edit',
              onEditClick: handleShowEditModal,
            }}
          />
        ) : (
          <Card>
            <EmptyState
              heading="No product found."
              action={{
                content: 'Add new product',
                onAction: handleShowAddModal,
              }}
              image="https://cdn.shopify.com/s/files/1/0262/4071/2726/files/emptystate-files.png"
            >
              <p>
                There is no product for this collection. You can add new
                product.
              </p>
            </EmptyState>
          </Card>
        )}

        {showAddModal ? (
          <CreateModal
            title="Add new product"
            onClose={handleHideAddModal}
            open={showAddModal}
            primaryActionHandler={handleAddProduct}
            primaryAction={{ content: 'Add' }}
            secondaryActions={[
              { content: 'Cancel', onAction: handleHideAddModal },
            ]}
          />
        ) : null}

        {showEditModal ? (
          <CreateModal
            title="Edit product"
            defaultName={selectedProduct?.name || ''}
            defaultDescription={selectedProduct?.description || ''}
            onClose={handleEditModalClose}
            open={showEditModal}
            primaryActionHandler={handleEditProduct}
            primaryAction={{ content: 'Edit' }}
            secondaryActions={[
              { content: 'Cancel', onAction: handleEditModalClose },
            ]}
          />
        ) : null}
      </Page>
      {showToast ? (
        <Toast
          error={toastErrorStatus}
          content={toastMessage}
          onDismiss={onToastDismiss}
          duration={3000}
        />
      ) : null}
    </Frame>
  );
};

export default Products;
