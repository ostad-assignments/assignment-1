export interface IUser {
  name: string;
  email: string;
  shopId: string;
  domain: string;
  password: string;
  installedAt: Date;
  passwordUpdatedAt: Date;
  uninstalledAt: Date;
}

export type IUserReqBody = Pick<IUser, 'domain' | 'password'>;
