import { useEffect, useState } from 'react';
import { getCollections } from '../service/collection';
import { ICollection } from '../utils/interfaces/collection.interface';

export const useGetCollectionList = () => {
  const [collections, setCollections] = useState<ICollection[]>([]);
  const [error, setError] = useState<null | string>(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    (async () => {
      try {
        const response = await getCollections();

        if (response.error) {
          setError(response.error);
        } else {
          setCollections(response.collections);
        }
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
      } catch (error: any) {
        setError(error?.message);
      } finally {
        setLoading(false);
      }
    })();
  }, []);

  return { collections, error, loading };
};
