import { useEffect, useState } from 'react';
import { getShopDetails } from '../service/shop';
import { IUser } from '../utils/interfaces/user.interface';

export const useShopInfo = () => {
  const [shop, setShop] = useState<IUser | null>(null);
  const [error, setError] = useState<null | string>(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    (async () => {
      try {
        const shop = await getShopDetails();

        if (shop.error) {
          setError(shop.error);
        } else {
          setShop(shop);
        }
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
      } catch (error: any) {
        setError(error?.message);
      } finally {
        setLoading(false);
      }
    })();
  }, []);

  return { shop, error, loading };
};
