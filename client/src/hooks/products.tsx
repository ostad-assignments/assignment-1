import { useEffect, useState } from 'react';
import { getProductsByCollection } from '../service/products';
import { IProduct } from '../utils/interfaces/product.interface';
import { ICollection } from '../utils/interfaces/collection.interface';

export const useGetProductsByCollection = (collectionId: string) => {
  const [products, setProducts] = useState<IProduct[]>([]);
  const [collection, setCollection] = useState<ICollection | null>(null);
  const [error, setError] = useState<null | string>(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    (async () => {
      try {
        setLoading(true);
        const response = await getProductsByCollection(collectionId);

        if (response.error) {
          setError(response.error);
        } else {
          setProducts(response.products);
          setCollection(response.collection);
        }
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
      } catch (error: any) {
        setError(error?.message);
      } finally {
        setLoading(false);
      }
    })();
  }, [collectionId]);

  return { products, loading, error, collection };
};
